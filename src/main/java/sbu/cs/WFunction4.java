package sbu.cs;

public class WFunction4 implements WhiteFunction {

    //return the first string if it's length is even. otherwise, return the second one.
    @Override
    public String function (String input1, String input2)
    {
        if(input1.length() % 2 == 0)
        {
            return input1;
        }

        return input2;
    }
}
