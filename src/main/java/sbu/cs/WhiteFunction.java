package sbu.cs;

public interface WhiteFunction {
    String function (String input1, String input2);
}
