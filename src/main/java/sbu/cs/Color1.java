package sbu.cs;

import java.util.ArrayList;

public class Color1 extends Color {
    protected BlackFunction func;
    private ArrayList<BlackFunction> functions;

    Color1(int number)
    {
        functions = new ArrayList<>();
        functions.add(new BFunction1());
        functions.add(new BFunction2());
        functions.add(new BFunction3());
        functions.add(new BFunction4());
        functions.add(new BFunction5());

        func = functions.get(number - 1);
    }

    @Override
    String getOutput()
    {
        String str = this.func.function(inputs.get(0));
        inputs.remove(0);
        inputs.trimToSize();
        return str;
    }
}
