package sbu.cs;

public class WFunction2 implements WhiteFunction {

    //reverse the second string and add it to the first one
    @Override
    public String function (String input1, String input2)
    {
        return input1 +
                new StringBuilder(input2).reverse();
    }
}
