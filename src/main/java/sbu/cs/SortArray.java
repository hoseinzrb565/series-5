package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                if (arr[i] > arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i = 1; i < size; i++) {
            int key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = key;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if(size < 2)
        {
            return arr;
        }

        int i;
        int mid = size / 2;
        int[] left = new int[mid];
        int[] right = new int[size - mid];

        for(i = 0; i < mid; i++)
        {
            left[i] = arr[i];
        }


        for(int j = 0; j < size - mid; j++)
        {
            right[j] = arr[i];
            i++;
        }

        mergeSort(left, mid);
        mergeSort(right, size - mid);
        merge(left, right, arr);
        return arr;
    }

    private void merge(int[] left, int[] right, int[] arr) {

        int i = 0, j = 0, k = 0;

        while(i < left.length && j < right.length)
        {
            if(left[i] <= right[j])
            {
                arr[k] = left[i];
                i++;
            }

            else
            {
                arr[k] = right[j];
                j++;
            }
            k++;
        }

        while(i < left.length)
        {
            arr[k] = left[i];
            i++;
            k++;
        }

        while(j < right.length)
        {
            arr[k] = right[j];
            j++;
            k++;
        }
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int right = arr.length - 1, left = 0, mid = (right + left) / 2;

        while(left <= right)
        {
            if(value == arr[mid])
            {
                return mid;
            }

            else if(value < arr[mid])
            {
                right = mid - 1;
            }

            else
            {
                left = mid + 1;
            }

            mid = (right + left) / 2;
        }

        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        return binarySearchAuxiliary(arr, 0, arr.length - 1 , value);
    }

    private int binarySearchAuxiliary(int[] arr, int left, int right, int value)
    {
        if(left > right)
        {
            return -1;
        }

        int mid = (right + left) / 2;

        if(value == arr[mid])
        {
            return mid;
        }

        else if(value < arr[mid])
        {
            return binarySearchAuxiliary(arr, left, mid - 1, value);
        }

        else
        {
            return binarySearchAuxiliary(arr, mid + 1, right, value);
        }
    }
}
