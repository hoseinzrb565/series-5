package sbu.cs;

public class BFunction4 implements BlackFunction {

    //shift each character one spot to the right
    @Override
    public String function(String input)
    {
        return input.charAt(input.length() - 1) +
                input.substring(0, input.length() - 1);
    }
}
