package sbu.cs;

public class WFunction1 implements WhiteFunction {

    //mix the characters(first to last, first to last)
    @Override
    public String function (String input1, String input2)
    {
        StringBuilder str = new StringBuilder();
        int i = 0, j = 0;

        while(i < input1.length() && j < input2.length())
        {
            str.append(input1.charAt(i));
            str.append(input2.charAt(j));
            i++;
            j++;
        }

        while(i < input1.length())
        {
            str.append(input1.charAt(i));
            i++;
        }

        while(j < input2.length())
        {
            str.append(input2.charAt(j));
            j++;
        }

        return str.toString();
    }
}
