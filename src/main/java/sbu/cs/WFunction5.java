package sbu.cs;

public class WFunction5 implements WhiteFunction {

    //mixing with alphabet arithmetic
    @Override
    public String function (String input1, String input2)
    {
        StringBuilder str = new StringBuilder();
        int i = 0, j = 0;

        while(i < input1.length() && j < input2.length())
        {
            str.append((char)('a' + (input1.charAt(i) - 'a' + input2.charAt(j) - 'a') % 26));
            i++;
            j++;
        }

        while(i < input1.length())
        {
            str.append(input1.charAt(i));
            i++;
        }

        while(j < input2.length())
        {
            str.append(input2.charAt(j));
            j++;
        }

        return str.toString();
    }
}
