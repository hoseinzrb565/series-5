package sbu.cs;

import java.util.ArrayList;

public class Color2 extends Color {
    protected WhiteFunction func;
    private ArrayList<WhiteFunction> functions;

    Color2(int number)
    {
        functions = new ArrayList<>();
        functions.add(new WFunction1());
        functions.add(new WFunction2());
        functions.add(new WFunction3());
        functions.add(new WFunction4());
        functions.add(new WFunction5());

        func = functions.get(number - 1);
    }

    @Override
    String getOutput()
    {
        return this.func.function(inputs.get(0), inputs.get(1));
    }

}
