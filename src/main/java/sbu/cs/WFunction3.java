package sbu.cs;

public class WFunction3 implements WhiteFunction {

    //mix the characters(first to last, last to first)
    @Override
    public String function (String input1, String input2)
    {
        StringBuilder str = new StringBuilder();
        int i = 0, j = input2.length() - 1;

        while(i < input1.length() && j >= 0)
        {
            str.append(input1.charAt(i));
            str.append(input2.charAt(j));
            i++;
            j--;
        }

        while(i < input1.length())
        {
            str.append(input1.charAt(i));
            i++;
        }

        while(j >= 0)
        {
            str.append(input2.charAt(j));
            j--;
        }

        return str.toString();
    }
}
