package sbu.cs;

import java.util.ArrayList;

public abstract class Color {
    protected ArrayList<String> inputs = new ArrayList<>();

    void addInput(String input){inputs.add(input);}
    abstract String getOutput();
}
