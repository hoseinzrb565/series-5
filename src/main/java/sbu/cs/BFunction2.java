package sbu.cs;

public class BFunction2 implements BlackFunction {

    //repeat each character
    @Override
    public String function(String input)
    {
        StringBuilder str = new StringBuilder();

        for(char c : input.toCharArray())
        {
            str.append(c);
            str.append(c);
        }

        return str.toString();
    }
}
