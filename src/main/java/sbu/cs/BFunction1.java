package sbu.cs;

public class BFunction1 implements BlackFunction {

    //reverse a string
    @Override
    public String function(String input)
    {
        return new StringBuilder(input).reverse().toString();
    }
}
