package sbu.cs;

public class BFunction5 implements BlackFunction {

    //alphabet arithmetic
    @Override
    public String function(String input)
    {
        int sum = 'a' + 'z';
        StringBuilder str = new StringBuilder();

        for(char c : input.toCharArray())
        {
            str.append((char)(sum - c));
        }

        return str.toString();
    }
}
