package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {

            Color[][] squares = new Color[n][n];
            int functionNumber;

            for(int i = 0; i < n; i++)
            {
                for(int j = 0; j < n; j++)
                {
                    functionNumber = arr[i][j];

                    if((j == n - 1 && i != 0) ||
                            (i == n - 1 && j != 0))
                    {
                        squares[i][j] = new Color2(functionNumber);
                    }

                    else
                    {
                        squares[i][j] = new Color1(functionNumber);
                    }

                }
            }

            squares[0][0].addInput(input);

            for(int j = 0; j < n; j++)
            {
                for(int i = 0; i < n; i++)
                {
                    String output = squares[i][j].getOutput();

                    //yellow or green
                    if(i == 0 || j == 0)
                    {
                        //top right(yellow)
                        if(j == n - 1)
                        {
                            squares[i + 1][j].addInput(output);
                        }

                        //bottom left(yellow)
                        else if(i == n - 1)
                        {
                            squares[i][j + 1].addInput(output);
                        }

                        //green
                        else
                        {
                            squares[i][j + 1].addInput(output);
                            squares[i + 1][j].addInput(output);
                        }
                    }

                    //blue
                    else if(i != n - 1 && j != n - 1)
                    {
                        String output2 = squares[i][j].getOutput();

                        squares[i][j + 1].addInput(output);
                        squares[i + 1][j].addInput(output2);
                    }

                    //pink
                    else
                    {
                        if(i == n - 1 && j != n - 1)
                        {
                            squares[i][j + 1].addInput(output);
                        }

                        else if(j == n - 1 && i != n - 1)
                        {
                            squares[i + 1][j].addInput(output);
                        }
                    }
                }
            }

            return squares[n - 1][n - 1].getOutput();
        }
    }
